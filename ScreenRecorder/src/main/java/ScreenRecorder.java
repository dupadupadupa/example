
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ScreenRecorder {

	Scanner scanner = new Scanner(System.in);

	List<Profile> lista = new ArrayList<>();
	List<Profile> active = new LinkedList<>();
	private boolean isRecording = false;

	public void add() {

		Profile profile = new Profile();

		for (int i = 0; i < 1; i++) {
			System.out.println("Wybierz kodek");
			System.out.println("1: H264");
			System.out.println("2: H263");
			System.out.println("3: THEORA");
			System.out.println("4: VR8");

			int input = scanner.nextInt();

			if (input < 5 && input > 0) {

				switch (input) {
				case 1:
					profile.setCodec(Codec.H264);
					break;
				case 2:
					profile.setCodec(Codec.H263);
					break;
				case 3:
					profile.setCodec(Codec.THEORA);
					break;
				case 4:
					profile.setCodec(Codec.VR8);
					break;
				}

			}

			else {
				System.out.println("Wprowadzono nieprawid�ow� warto��");
				System.out.println("");
			}

			if (input == 0 || input >= 5) {
				i = i - 1;
			}

		}

		for (int i = 0; i < 1; i++) {
			System.out.println("Wybierz rozszerzenie");
			System.out.println("1: MP4");
			System.out.println("2: MP3");
			System.out.println("3: MKV");
			System.out.println("4: AVI");

			int input = scanner.nextInt();

			if (input < 5 && input > 0) {

				switch (input) {
				case 1:
					profile.setExtension(Extension.MP4);
					break;
				case 2:
					profile.setExtension(Extension.MP3);
					break;
				case 3:
					profile.setExtension(Extension.MKV);
					break;
				case 4:
					profile.setExtension(Extension.AVI);
					break;
				}
			} else {
				System.out.println("Wprowadzono nieprawid�ow� warto��");
				System.out.println("");
			}

			if (input == 0 || input >= 5) {
				i = i - 1;
			}
		}

		for (int i = 0; i < 1; i++) {
			System.out.println("Wybierz rozdzielczo��");
			System.out.println("1: FULLHD");
			System.out.println("2: HD");
			System.out.println("3: VGA");

			int input = scanner.nextInt();

			if (input < 4 && input > 0) {

				switch (input) {
				case 1:
					profile.setResolution(Resolution.FULLHD);
					break;
				case 2:
					profile.setResolution(Resolution.HD);
					break;
				case 3:
					profile.setResolution(Resolution.VGA);
					break;

				}
			} else {
				System.out.println("Wprowadzono nieprawid�ow� warto��");
				System.out.println("");
			}

			if (input == 0 || input >= 4) {
				i = i - 1;
			}
		}
		lista.add(profile);
		System.out.println("\n");
		menu();
	}

	public void prntList() {
		int index = 1;
		if (!lista.isEmpty()) {
			for (Profile p : lista) {
				System.out.println(index + ". " + p.getCodec() + ", " + p.getExtension() + ", " + p.getResolution());
				index++;
			}
		} else {
			System.out.println("Nie dodano �adnego profilu");
		}
		System.out.println("\n");
		menu();

	}

	public void setProfile() {
		for (int i = 0; i < 1; i++) {

			System.out.println("Podaj numer profilu, kt�ry ma zosta� ustawiony jako aktywny");
			int index = scanner.nextInt() - 1;
			if (lista.get(index) != null) {
				active.add(lista.get(index));
			}

			else if (lista.get(index) == null) {
				i = i - 1;
			}

			else if (lista.isEmpty()) {
				add();
			}
		}

		System.out.println("\n");
		menu();
		}

	public void startRecording() {
		if (!active.isEmpty()) {
			System.out.println("Nagrywanie");

			scanner.next();
			isRecording = true;
			stopRecording();
		}

		else {
			System.out.println("Nie wybrano �adnego profilu jako aktywny");
			setProfile();
		}

	}

	public void stopRecording() {
		if(isRecording == false){
			System.out.println("Nic si� nie nagrywa");
			System.out.println("\n");
			menu();
		}
		else{
		
		System.out.println("Przerwano nagrywanie");

		for (int i = 0; i < 1; i++) {

			System.out.println("1 - wr�� do wyboru profilu");
			System.out.println("2 - nagraj ponownie");
			int input = scanner.nextInt();

			if (input < 3 && input > 0) {

				switch (input) {
				case 1:
					active.remove(0);
					setProfile();
					break;
				case 2:
					startRecording();
					break;
				}

			}

			else {
				System.out.println("Wprowadzono nieprawid�ow� warto��");
				System.out.println("");
			}

			if (input == 0 || input >= 3) {
				i = i - 1;
			}

		}
		System.out.println("\n");
		menu();
		}
	}

	public void menu(){
		for (int i = 0; i < 1; i++) {
			System.out.println("Wybierz opcj�: ");
	        System.out.println("1 - dodaj profil");
	        System.out.println("2 - poka� dost�pne profile");
	        System.out.println("3 - wybierz profil");
	        System.out.println("4 - rozpocznij nagrywanie");
	        System.out.println("5 - zatrzymaj nagrywanie");
			int input = scanner.nextInt();

			if (input < 6 && input > 0) {

				switch (input) {
				case 1:
					add();
					break;
				case 2:
					prntList();
					break;
				case 3:
					setProfile();
					break;
				case 4:
					startRecording();
					break;
				case 5:
					stopRecording();
					break;
				}

			}

			else {
				System.out.println("Wprowadzono nieprawid�ow� warto��");
				System.out.println("");
			}

			if (input == 0 || input >= 6) {
				i = i - 1;
			}

		}
	}
	
}
