
public class Profile {
private Codec codec;
private Extension extension;
private Resolution resolution;


public Profile(Codec codec, Extension extension, Resolution resolution) {
	super();
	this.codec = codec;
	this.extension = extension;
	this.resolution = resolution;
}


public Profile() {
	
}



public Codec getCodec() {
	return codec;
}

public void setCodec(Codec codec) {
	this.codec = codec;
}

public Extension getExtension() {
	return extension;
}

public void setExtension(Extension extension) {
	this.extension = extension;
}

public Resolution getResolution() {
	return resolution;
}

public void setResolution(Resolution resolution) {
	this.resolution = resolution;
}


}
