
public enum Resolution {
	FULLHD (1920, 1080), HD(1280, 720), VGA(640, 480);
	
	private int h;
	
	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	private int w;

	Resolution(int w, int h){
		this.w = w;
		this.h = h;
	}
}
